__author__ = 'jorgeramos'
import os
from setuptools import setup

README = open(os.path.join(os.path.dirname(__file__), 'README.rst')).read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name = "notifications",
    version = "0.0.3",
    author = "Jorge Alpedrinha Ramos",
    author_email = "jalpedrinharamos@gmail.com",
    description = ("An initial approach to create a notification system based on django-activity-stream"),
    packages = ["notifications"],
    license='BSD License',  # example license
    long_description=README,
    url='http://bitbucket.org/jalpedrinha/notifications',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # replace these appropriately if you are using Python 3
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.7',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
    install_requires=[
        "Django == 1.5.1",
        "django-activity-stream == 0.4.5beta1"
    ],
)
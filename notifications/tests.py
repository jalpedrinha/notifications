# _*_  encoding: utf-8 _*_
"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""
from datetime import date
from actstream.models import following, Follow, Action, actor_stream
from django.db.models.signals import post_save

from django.test import TestCase
from django.core.management import call_command
import sys
from appschema.schema import schema_store
from common.models import Terceiro
from evaluation.factories import TesteFactory
from evaluation.models import Teste
from notifications import generators
from pedagogico.factories import MatriculaFactory, TurmaDisciplinaFactory, MatriculaDisciplinaFactory, TurmaDisciplinaProfessorFactory
from pedagogico.models import Aluno, TurmaDisciplina, Matricula
from pedagogico.notification import CreateTestGenerator, FollowStudentEnrollSubject, FollowChangeEnrollSubject
from pessoal.models import Empregado
from notifications import registry

class UpdateFollowTestCase(TestCase):

    fixtures = ['terceiros', 'alunos', 'pessoal', 'anolectivo',
                'ano', 'curso', 'anolectivoescola', 'cursoanoanolectivo']

    def setUp(self):
        " Test my custom command."
        self.setUpForStudent()
        self.setUpForTeacher()
        self.setUpForClassDir()
        Follow.objects.all().delete()
        call_command('update_follows', schema='teste', verbosity=None)
        self.assertTrue(True,'update')

    def setUpForStudent(self):
        self.student = Aluno.objects.all()[0]
        self.enroll = MatriculaFactory.create(aluno = self.student)
        self.subject = TurmaDisciplinaFactory.create()
        self.subject_enroll = MatriculaDisciplinaFactory.create(matricula = self.enroll, turma = self.subject)

    def setUpForTeacher(self):
        self.teacher = Empregado.objects.filter(tipo='D')[0]
        self.subject_teacher = TurmaDisciplinaProfessorFactory(turma_disciplina = self.subject, teacher = self.teacher)

    def setUpForClassDir(self):
        self.director = Empregado.objects.filter(tipo='D')[1]
        self.enroll.turma = self.subject.turma
        self.enroll.save()
        self.subject.turma.director = self.director
        self.subject.turma.save()

    def test_follows_created(self):
        count = Follow.objects.all().count()
        self.assertTrue(count > 0)

    def test_student_follows_subject(self):
        self.assertIn(self.subject, following(self.student.terceiro_ptr, TurmaDisciplina), 'Student not following subject')

    def test_student_follows_enroll(self):
        self.assertIn(self.enroll, following(self.student.terceiro_ptr, Matricula), 'Student not following subject')

    def test_teacher_follows_teaching_subject(self):
        self.assertIn(self.subject, following(self.teacher.terceiro_ptr, TurmaDisciplina), 'Teacher not following subject')

    def test_director_follows_teacher(self):
        self.assertIn(self.teacher.terceiro_ptr, following(self.director.terceiro_ptr, Terceiro), 'Director not following Teachers')

    def test_director_follows_students(self):
        self.assertIn(self.enroll, following(self.director.terceiro_ptr, Matricula), 'Director not following Enroll')

    def test_director_follows_subject(self):
        self.assertIn(self.subject, following(self.director.terceiro_ptr, TurmaDisciplina), 'Director not following Subjects')

class C(object):
    d = True
class B(object):
    c = C()
class A(object):
    b = B()

class TestGenerators(TestCase):

    fixtures = ['terceiros', 'alunos', 'pessoal', 'anolectivo',
                'ano', 'curso', 'anolectivoescola', 'cursoanoanolectivo']
    def setUp(self):
        self.setUpForStudent()
        self.setUpForTeacher()
        self.setUpForClassDir()

    def setUpForStudent(self):
        self.student = Aluno.objects.all()[0]
        self.enroll = MatriculaFactory.create(aluno = self.student)
        self.subject = TurmaDisciplinaFactory.create()
        self.subject_enroll = MatriculaDisciplinaFactory.create(matricula = self.enroll, turma = self.subject)

    def setUpForTeacher(self):
        self.teacher = Empregado.objects.filter(tipo='D')[0]
        self.subject_teacher = TurmaDisciplinaProfessorFactory(turma_disciplina = self.subject, teacher = self.teacher)

    def setUpForClassDir(self):
        self.director = Empregado.objects.filter(tipo='D')[1]
        self.enroll.turma = self.subject.turma
        self.enroll.save()
        self.subject.turma.director = self.director
        self.subject.turma.save()

    def test_get_value_from_accessor(self):
        self.assertTrue(generators.get_value_from_accessor(C(),'d'), 'Failed value from accessor at first level')
        self.assertTrue(generators.get_value_from_accessor(B(),'c__d'), 'Failed value from accessor at second level')
        self.assertTrue(generators.get_value_from_accessor(A(),'b__c__d'), 'Failed value from accessor at third level')

    def test_notification_on_create_generator(self):
        registry.register(CreateTestGenerator)
        test = TesteFactory.create(turma_disciplina= self.subject, obs = 'abc')
        self.assertTrue(len(actor_stream(test.turma_disciplina)) > 0)
        actor_stream(test.turma_disciplina).delete()
        print [x[1] for x in post_save.receivers]
        registry.unregister(CreateTestGenerator)
        test = TesteFactory.create(turma_disciplina= self.subject, obs = 'abc')
        print actor_stream(test.turma_disciplina)
        self.assertTrue(len(actor_stream(test.turma_disciplina)) == 0)

    def test_follow_on_create_generator(self):
        registry.register(FollowStudentEnrollSubject)
        subject = TurmaDisciplinaFactory.create()
        MatriculaDisciplinaFactory.create(matricula = self.enroll, turma = subject)
        self.assertIn(subject, following(self.enroll.aluno.terceiro_ptr, TurmaDisciplina))
        registry.unregister(FollowStudentEnrollSubject)
        subject2 = TurmaDisciplinaFactory.create()
        MatriculaDisciplinaFactory.create(matricula = self.enroll, turma = subject2)
        self.assertNotIn(subject2,following(self.enroll.aluno.terceiro_ptr, TurmaDisciplina))

    def test_follow_on_change_generator(self):
        registry.register(FollowChangeEnrollSubject)
        subject = TurmaDisciplinaFactory.create()
        enroll_subject = MatriculaDisciplinaFactory.create(matricula = self.enroll, turma = subject)
        subject2 = TurmaDisciplinaFactory.create()
        enroll_subject.turma = subject2
        enroll_subject.save()
        self.assertIn(subject2, following(self.enroll.aluno.terceiro_ptr, TurmaDisciplina))
        registry.unregister(FollowChangeEnrollSubject)
        subject3 = TurmaDisciplinaFactory.create()
        enroll_subject.turma = subject3
        enroll_subject.save()
        self.assertNotIn(subject3,following(self.enroll.aluno.terceiro_ptr, TurmaDisciplina))


class SimpleTest(TestCase):
    def test_basic_addition(self):
        """
        Tests that 1 + 1 always equals 2.
        """
        self.assertEqual(1 + 1, 2)

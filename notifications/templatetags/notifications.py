# _*_ encoding: utf-8  _*_

from django.template import Library, loader, Context
from django.contrib.admin.templatetags.admin_static import static
from django.template.defaultfilters import stringfilter
from django.utils.html import format_html
from django.utils.text import capfirst
from django.utils.encoding import force_text
from django.utils.translation import ugettext_lazy as _
from django.core.serializers import serialize
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse


register = Library()
@register.filter
@stringfilter
def upto(value, delimiter=None):
    return value.split(delimiter)[0]
upto.is_safe = True

@register.simple_tag(takes_context=True)
def notification(context, action=None):
    if not action:
        action = context['action']
    template = ""
    if action.data and 'template' in action.data:
        if action.data['template']:
            template = [action.data['template']]

    if not template:
        template = ["action.html"]

    t = loader.select_template(template)
    context.update({'action' : action})
    return t.render(context)

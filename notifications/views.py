# Create your views here.
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from notifications.models import user_notifications
import json

@login_required(login_url='/')
def update_notification(request):
    request.user.update_notifications()

    return HttpResponse(json.dumps({'success': True}), content_type="application/json")

@login_required(login_url='/')
def view_notifications(request):
    notifications = user_notifications(request.user)
    request.user.update_notifications()
    return render_to_response('notifications/view_all.html', { 'notifications': notifications}, RequestContext(request))


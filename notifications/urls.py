__author__ = 'jorgeramos'
try:
    from django.conf.urls import url, patterns
except ImportError:
    from django.conf.urls.defaults import url, patterns
from notifications import views

urlpatterns = patterns('',
    # Syndication Feeds
    url(r'^view_all/$', views.view_notifications
        , name='view_notifications'),
    url(r'^update/$', views.update_notification
        , name='update_viewed'),
)

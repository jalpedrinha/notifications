from actstream import action
import sys
from actstream.actions import follow, unfollow
from django.db.models.signals import post_save, pre_save, post_delete

__author__ = 'jorgeramos'

def get_value_from_accessor(instance, accessor):
    if not accessor:
        return None
    elif accessor == 'self':
        return instance

    if '__' in accessor:
        accessors = accessor.split('__')
    else:
        accessors = [accessor]

    accessor = accessors[0]
    if hasattr(instance, accessor):
        instance = getattr(instance,accessor, None)
    else:
        return None
    if len(accessors) > 1:
        instance = get_value_from_accessor(instance, '__'.join(accessors[1:]))
    return instance


class HandlerGenerator(object):
    model = None
    signal = None
    run_initially = False
    def action(self, instance, **kwargs):
        pass

    def get_handler(self):
        def follow_object(sender, instance, **kwargs):
            self.action(instance, **kwargs)
        return follow_object

    def connect(self):
        self.signal.connect(self.get_handler(), sender= self.model, weak = False, dispatch_uid=type(self).__name__)

    def disconnect(self):
        self.signal.disconnect(sender= self.model, dispatch_uid=type(self).__name__)


class FollowGenerator(HandlerGenerator):
    user_accessor = 'self'
    object_accessor = None

    def get_user(self, instance):
        return get_value_from_accessor(instance, self.user_accessor)

    def get_object(self, instance):
        return get_value_from_accessor(instance, self.object_accessor)


class FollowOnCreate(FollowGenerator):
    run_initially = True
    signal = post_save
    def get_user(self, instance):
        return get_value_from_accessor(instance, self.user_accessor)

    def get_object(self, instance):
        return get_value_from_accessor(instance, self.object_accessor)

    def initial_follows(self):
        instances = self.model.objects.all()
        for instance in instances:
            kwargs = {'created' : True}
            self.action(instance,**kwargs)

    def action(self, instance, **kwargs):
        if kwargs.get('created', False):
            user = self.get_user(instance)
            object = self.get_object(instance)
            if user and object:
                follow(user, object)


class FollowOnChange(FollowGenerator):
    signal = pre_save
    attribute = None

    def is_dirty_attribute(self, instance):
        return instance.is_dirty(self.attribute)

    def action(self, instance, **kwargs):
        if self.is_dirty_attribute(instance):
            user = self.get_user(instance)
            object = self.get_object(instance)
            if user and object:
                follow(user, object)


class UnFollowOnChange(FollowGenerator):
    signal = pre_save
    attribute = None

    def is_dirty_attribute(self, instance):
        return instance.is_dirty(self.attribute)

    def action(self, instance, **kwargs):
        if self.is_dirty_attribute(instance):
            user = self.get_user(instance.db_instance)
            object = self.get_object(instance.db_instance)
            if user and object:
                unfollow(user, object)


class UnFollowOnDelete(FollowGenerator):
    signal = post_delete

    def action(self, instance, **kwargs):
        user = self.get_user(instance)
        object = self.get_object(instance)
        if user and object:
            unfollow(user, object)


class NotificationGenerator(HandlerGenerator):
    actor_accessor = 'self'
    verb = 'action'
    target_accessor = None
    action_object_accessor = None
    extra_args = {}

    def get_target(self, instance):
        print 'target', get_value_from_accessor(instance, self.target_accessor)
        return get_value_from_accessor(instance, self.target_accessor)

    def get_action_object(self, instance):
        print 'action_object', self.action_object_accessor, get_value_from_accessor(instance, self.action_object_accessor)
        return get_value_from_accessor(instance, self.action_object_accessor)

    def get_actor(self, instance):
        return get_value_from_accessor(instance, self.actor_accessor)

    def get_extra_args(self, instance):
        return self.extra_args

    def action(self, instance, **kwargs):
        target = self.get_target(instance)
        action_object = self.get_action_object(instance)
        actor = self.get_actor(instance)
        if actor:
            action.send(actor, verb = self.verb, action_object = action_object, target=target, **self.get_extra_args(instance))

class NotificationOnCreate(NotificationGenerator):
    run_initially = True
    signal = post_save
    def get_handler(self):
        def send_notification_on_create(sender, instance, **kwargs):
            if kwargs.get('created',False):
                self.action(instance, **kwargs)
        return send_notification_on_create

class NotificationOnDelete(NotificationGenerator):
    run_initially = True
    signal = post_delete

class NotificationOnChange(NotificationGenerator):
    run_initially = True
    signal = pre_save
    attribute = None
    check_value = None
    def get_handler(self):
        def follow_object(sender, instance, **kwargs):
            if hasattr(instance.db_instance,self.attribute):
                if getattr(instance.db_instance,self.attribute) != getattr(instance, self.attribute):
                    if self.check_value is None or getattr(instance, self.attribute) == self.check_value:
                        self.action(instance, **kwargs)
        return follow_object
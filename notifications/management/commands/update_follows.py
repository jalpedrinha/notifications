# _*_  encoding: utf-8 _*_

from optparse import make_option
from appschema.schema import schema_store
from django.core.management import BaseCommand
from notifications.generators import FollowGenerator

__author__ = 'jorgeramos'

class Command(BaseCommand):
    args = '<poll_id poll_id ...>'
    help = 'Closes the specified poll for voting'
    option_list = BaseCommand.option_list + (
        make_option('--schema', action='store', dest='schema',
            default='', help='schema to update'),
    )
    def handle(self, *args, **options):
        import notifications
        notifications.autodiscover()
        verbosity = options.get('verbosity', None)
        schema = options.get('schema')
        schema_store.schema= schema
        schema_store.set_path()

        from notifications import registry
        for generator in [x() for x in registry.notification_generators if isinstance(x(),FollowGenerator) and x().run_initially]:
            print generator.__class__
            generator.initial_follows()

        #persons = Terceiro.objects.all()
        #n = persons.count()
        #counter = 1
        #if verbosity:
        #    sys.stdout.write('updating')
        #for person in persons:
        #    if verbosity:
        #        print u'{} of {} ...'.format(counter,n)
        #    if hasattr(person, 'aluno'):
        #        matriculas = Matricula.objects.filter(aluno = person.aluno)
        #        for matricula in matriculas:
        #            follow(person, matricula, send_action = False )
        #            for turmadisciplina in matricula.matriculadisciplina_set.all():
        #                follow(person, turmadisciplina.turma, send_action = False )
        #    if hasattr(person, 'empregado') and person.tipo == 'D':
        #        tds = TurmaDisciplinaProfessor.objects.filter(teacher=person.empregado)
        #        for subject in [x.turma_disciplina for x in tds]:
        #            follow(person, subject, send_action = False )
        #        directions = Turma.objects.filter(director = person.empregado)
        #        for class_group in directions:
        #            enrolls = class_group.matricula_set.all()
        #            for enroll in enrolls:
        #                follow(person, enroll, send_action = False )
        #            tds_director = class_group.turmadisciplina_set.all()
        #            for subject in tds_director:
        #                follow(person, subject, send_action = False )
        #                for teacher in subject.professores.all():
        #                    follow(person, teacher.teacher.terceiro_ptr, send_action = False )
        #    counter += 1

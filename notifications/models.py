from actstream.models import user_stream
from django.template.loader import render_to_string
from django.utils.safestring import mark_safe


class CustomNotification(object):

    def __init__(self, action, short_display = False):
        self.action = action
        self.short_display = short_display

    def get_template(self):
        if self.action.data and 'template' in self.action.data:
            return self.action.data.get('template')
        else:
            return 'notifications/action.html'

    def __unicode__(self):
        context = {'action' : self.action, 'short_display' : self.short_display}
        rendered = render_to_string(self.get_template(), context)
        return mark_safe(rendered)


def user_notification_digest(user):
    stream = user_stream(user)
    recent = stream.filter(timestamp__gt = user.last_notifications_update)

    notifications = stream[:5]
    user_nots = []
    for notification in notifications:
        user_nots.append(CustomNotification(notification, True))

    return {
        'messages' : user_nots,
        'has_more' : len(stream) > 5,
        'new' : len(recent) > 0 and len(recent),
    }

def user_notifications(user):
    notifications = user_stream(user)
    user_nots = []
    for notification in notifications:
        user_nots.append(CustomNotification(notification))
    return user_nots



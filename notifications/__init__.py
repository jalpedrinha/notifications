__author__ = "jorgeramos"

from generators import HandlerGenerator, NotificationOnCreate


def autodiscover():
    """
    Auto-discover INSTALLED_APPS admin.py modules and fail silently when
    not present. This forces an import on them to register any admin bits they
    may want.
    """

    import copy
    from django.conf import settings
    from django.utils.importlib import import_module
    from django.utils.module_loading import module_has_submodule

    for app in settings.INSTALLED_APPS:
        mod = import_module(app)
        # Attempt to import the app's admin module.

        try:
            import_module('%s.notification' % app)
        except:
            # Decide whether to bubble up this error. If the app just
            # doesn't have an admin module, we can ignore the error
            # attempting to import it, otherwise we want it to bubble up.
            if module_has_submodule(mod, 'notification'):
                raise


class Registry(object):
    notification_generators = []

    def register(self, generator):
        self.notification_generators.append(generator)
        generator = generator()
        generator.connect()

    def unregister(self, generator):
        self.notification_generators.remove(generator)
        generator = generator()
        generator.disconnect()


registry = Registry()